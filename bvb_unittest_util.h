/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef BVB_UNITTEST_UTIL_H_
#define BVB_UNITTEST_UTIL_H_

#include <inttypes.h>

#include <gtest/gtest.h>

#include <base/files/file_util.h>
#include <base/strings/string_util.h>
#include <base/strings/stringprintf.h>

/* Utility macro to run the command expressed by the printf()-style string
 * |command_format| using the system(3) utility function. Will assert unless
 * the command exits normally with exit status |expected_exit_status|.
 */
#define EXPECT_COMMAND(expected_exit_status, command_format, ...) do { \
  int rc = system(base::StringPrintf(command_format, ## __VA_ARGS__).c_str()); \
  EXPECT_TRUE(WIFEXITED(rc)); \
  EXPECT_EQ(WEXITSTATUS(rc), expected_exit_status); \
} while (0);

/* Base-class used for unit test. */
class BaseBvbToolTest : public ::testing::Test {
public:
  BaseBvbToolTest() {}

protected:
  virtual ~BaseBvbToolTest() {}

  /* Generates a Brillo Boot Image, using bvbtoool. The generated boot
   * image will written to disk, see the |boot_image_path| variable
   * for its path and |boot_image_| for the content.
   */
  void GenerateBootImage(const std::string& algorithm,
                         const std::string& kernel_cmdline,
                         uint64_t rollback_index,
                         const base::FilePath& key_path,
                         const std::string& additional_options = "") {
    boot_image_path_ = testdir_.Append("boot_brillo.img");
    EXPECT_COMMAND(0,
                   "./bvbtool make_boot_image"
                   " --kernel %s"
                   " --initrd %s"
                   " --kernel_cmdline \"%s\""
                   " --rollback_index %" PRIu64
                   " %s "
                   " --output %s",
                   base::FilePath("test/dummy_kernel.bin").value().c_str(),
                   base::FilePath("test/dummy_initrd.bin").value().c_str(),
                   kernel_cmdline.c_str(),
                   rollback_index,
                   additional_options.c_str(),
                   boot_image_path_.value().c_str());
    if (algorithm != "") {
      EXPECT_COMMAND(0,
                     "./bvbtool sign_boot_image --key %s"
                     " --image %s --algorithm %s",
                     key_path.value().c_str(),
                     boot_image_path_.value().c_str(),
                     algorithm.c_str());
    }
    int64_t file_size;
    ASSERT_TRUE(base::GetFileSize(boot_image_path_, &file_size));
    boot_image_.resize(file_size);
    ASSERT_TRUE(base::ReadFile(boot_image_path_,
                               reinterpret_cast<char*>(boot_image_.data()),
                               boot_image_.size()));
  }

  /* Create temporary directory to stash images in. */
  virtual void SetUp() override {
    base::FilePath ret;
    char* buf = strdup("/tmp/bvb-refimpl-tests.XXXXXX");
    ASSERT_TRUE(mkdtemp(buf) != nullptr);
    testdir_ = base::FilePath(buf);
    free(buf);
  }

  /* Nuke temporary directory. */
  virtual void TearDown() override {
    ASSERT_EQ(0U, testdir_.value().find("/tmp/bvb-refimpl-tests"));
    ASSERT_TRUE(base::DeleteFile(testdir_, true /* recursive */));
  }

  /* Temporary directory created in SetUp(). */
  base::FilePath testdir_;

  /* Path to boot image generated with GenerateBootImage(). */
  base::FilePath boot_image_path_;

  /* Contents of the image generated with GenerateBootImage(). */
  std::vector<uint8_t> boot_image_;
};

#endif  /* BVB_UNITTEST_UTIL_H_ */
